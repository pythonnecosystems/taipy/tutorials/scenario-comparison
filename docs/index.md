# 시나리오 비교

> [여기](https://docs.taipy.io/scenario_comparison.py)에서 코드를 다운로드받을 수 있다.

![](./config.svg)

Taipy는 시나리오 구성에 함수를 직접 포함시켜 시나리오 간 데이터 노드를 비교할 수 있는 방법을 제공한다.

## 시나리오 비교하기

**STep 1**: 비교 함수를 적용할 데이터 노드를 선언한다.

이 예에서는 '`output`' 데이터 노드를 비교하는 것을 적용하려고 한다. `configure_scenario`의 comparators 파라미터에 표시한다. 

```python
scenario_cfg = Config.configure_scenario(id="multiply_scenario",
                                         name="my_scenario",
                                         tasks_configs=[first_task_cfg,
                                                        second_task_cfg],
                                         comparators={output_data_node_cfg.id: compare_function})
```

**Step 2**: 위에서 사용한 비교 함수(`compare_function()`)을 구현한다.

`data_node_results`는 비교기(comparator)를 통과한 모든 시나리오의 Output Data Node 리스트이다. 시나리오를 비교하기 위해 반복한다.

```python
def compare_function(*data_node_results):
    compare_result = {}
    current_res_i = 0
    for current_res in data_node_results:
        compare_result[current_res_i] = {}
        next_res_i = 0
        for next_res in data_node_results:
            print(f"comparing result {current_res_i} with result {next_res_i}")
            compare_result[current_res_i][next_res_i] = next_res - current_res
            next_res_i += 1
        current_res_i += 1
    return compare_result
```

이제 Taipy 내에서 `compare_scenario`를 사용할 수 있다.

```python
tp.Core().run()

scenario_1 = tp.create_scenario(scenario_cfg)
scenario_2 = tp.create_scenario(scenario_cfg)

scenario_1.submit()
scenario_2.submit()

print(tp.compare_scenarios(scenario_1, scenario_2))
```

출력:

```
...
{'output': {'compare_function': {0: {0: 0, 1: -4}, 1: {0: 4, 1: 0}}}}
```

## 전체 코드

```python
from taipy.core.config import Config, Frequency
import taipy as tp


# Normal function used by Taipy
def double(nb):
    return nb * 2

def add(nb):
    return nb + 10


# Configuration of Data Nodes
input_cfg = Config.configure_data_node("input", default_data=21)
intermediate_cfg = Config.configure_data_node("intermediate")
output_cfg = Config.configure_data_node("output")

# Configuration of tasks
first_task_cfg = Config.configure_task("double",
                                    double,
                                    input_cfg,
                                    intermediate_cfg)

second_task_cfg = Config.configure_task("add",
                                    add,
                                    intermediate_cfg,
                                    output_cfg)



def compare_function(*data_node_results):
    # example of function
    compare_result = {}
    current_res_i = 0
    for current_res in data_node_results:
        compare_result[current_res_i] = {}
        next_res_i = 0
        for next_res in data_node_results:
            print(f"comparing result {current_res_i} with result {next_res_i}")
            compare_result[current_res_i][next_res_i] = next_res - current_res
            next_res_i += 1
        current_res_i += 1
    return compare_result


scenario_cfg = Config.configure_scenario(id="multiply_scenario",
                                         name="my_scenario",
                                         task_configs=[first_task_cfg, second_task_cfg],
                                         comparators={output_cfg.id: compare_function})


if __name__=="__main__":
    tp.Core().run()

    scenario_1 = tp.create_scenario(scenario_cfg)
    scenario_2 = tp.create_scenario(scenario_cfg)

    scenario_1.input.write(10)
    scenario_2.input.write(8)

    scenario_1.submit()
    scenario_2.submit()

    print(tp.compare_scenarios(scenario_1, scenario_2))
```